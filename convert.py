#!/usr/bin/env python3

import pandas as pd
import datetime
import sys

#import platform
#print(platform.python_version())

if len(sys.argv) != 3:
    print('Usage: ' + sys.argv[0] + ' year month')
    sys.exit(1)

# params month number and year
year = sys.argv[1]
#print('y:', year)
month_number = sys.argv[2]

month_names = {'1': 'jan', '2': 'feb', '3': 'mar', '4': 'apr', '5': 'may', '6': 'jun', '7': 'jul', '8': 'aug', '9': 'sep', '10': 'oct',
               '11': 'nov', '12': 'dec'}
month_as_str = month_names[month_number]
month_number = '{:02}'.format(int(month_number))
#print('m:', month_as_str)
#sys.exit(0)

df = pd.read_csv('../' + month_as_str + '-' + year + '.csv')

df.Postcode.fillna(0, inplace=True)
df.Postcode = df.Postcode.astype('int').astype('str')

df.PriceUpdatedDate = pd.to_datetime(df.PriceUpdatedDate, infer_datetime_format=True).dt.normalize()

for month in range(1, 13):
    old_date_as_string = year + '-{:02}-' + month_number
    old_date_as_string = old_date_as_string.format(month)
    new_date_as_string = year + '-' + month_number + '-{:02}'.format(month)
    #print(old_date_as_string)
    #print(new_date_as_string)
    #sys.exit(0)
    df.loc[df.PriceUpdatedDate == pd.Timestamp(old_date_as_string), 'PriceUpdatedDate'] = pd.Timestamp(new_date_as_string)

my_list = []

Address = None
Suburb = None
Postcode = None
PriceUpdatedDate = None

for index, row in df.iterrows():
    
    if isinstance(row.Address, str):
        Address = row.Address
        Suburb = row.Suburb
        Postcode = row.Postcode
        PriceUpdatedDate = row.PriceUpdatedDate

    if row.FuelCode == 'P98':
        my_list.append({'Address': Address, 'Suburb': Suburb, 'Postcode': Postcode,
                        'PriceUpdatedDate': PriceUpdatedDate, 'Price': int(row.Price * 10)})

new_df = pd.DataFrame(my_list)
df_for_csv = new_df.groupby('PriceUpdatedDate')['Price'].mean().apply(lambda x: round(x))
df_for_csv.to_csv(month_as_str + '-' + year + '-sum.csv', header=False)
